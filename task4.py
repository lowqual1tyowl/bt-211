import random
n = [[random.randrange(0,100) for i in range(2)] for j in range(10)]
print(n, end="\t")

import csv
FILENAME = 'user.csv'
users = [n]
with open(FILENAME, "w", newline="") as file:
    writer=csv.writer(file)
    writer.writerows(users)
FILENAME = "user.csv"

with open(FILENAME, "r", newline="") as file:
    reader = csv.reader(file)
    for row in reader:
        print(row[0], " - ", row[1])

